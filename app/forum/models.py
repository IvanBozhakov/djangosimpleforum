# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Category(models.Model):
    category_name = models.CharField(max_length=255)
    category_description = models.TextField()

    def __unicode__(self):
        return '%s' % (self.category_name)

class Topic(models.Model):
    category = models.ForeignKey(Category)
    author = models.ForeignKey(User)
    title = models.CharField(max_length=255)
    text = models.TextField()
    pub_date = models.DateTimeField()

    def __unicode__(self):
        return '%s' % (self.title)

class SubTopic(models.Model):
    topic = models.ForeignKey(Topic,related_name='subtopic')
    author = models.ForeignKey(User)
    text = models.TextField()
    pub_date = models.DateTimeField()

    def __unicode__(self):
        return '%s' % (self.text)


class Avatar(models.Model):
    user = models.ForeignKey(User)
    image = models.ImageField(upload_to='images')

    def __unicode__(self):
        return '%s' % (self.image)