from django.conf.urls import url
from forum import auth
from forum import views
from django.conf import settings



urlpatterns = [
    url(r'^topic/create', views.create_topic),
    url(r'^subtopic/create', views.create_subtopic),
    url(r'^topic/(?P<topic_id>[0-9]+)/$', views.show_topic),
]