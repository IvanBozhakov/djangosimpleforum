# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from models import Category
from django.utils.timezone import now
from models import Topic, Category,SubTopic, Avatar
from django.shortcuts import redirect
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator
from forms import ProfileForm
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required





# Create your views here.
def index(request):
    categories = Category.objects.all()
    topics = Topic.objects.all()
    return render(request,"index.html",{"categories":categories,"topics":topics})

#create new topic

def create_topic(request):
    if request.user.is_authenticated:
        title = request.POST.get('title', '')
        text = request.POST.get('text','')
        category_id = request.POST.get('category','')
        category = Category.objects.get(id=category_id)
        try:
            if not category:
                raise ValueError("No selected category")
            if len(title.strip()) == 0:
                raise ValueError("The field title is required")
            topic = Topic()
            topic.author = request.user
            topic.pub_date = now()
            topic.title = title
            topic.text = text
            topic.category = category
            topic.save()
            messages.add_message(request, messages.SUCCESS, "Добавихте успешно нова тема")
        except ValueError as e:
            messages.add_message(request, messages.WARNING, str(e[0]))

        return redirect("/")

#show single topic
def show_topic(request,topic_id):
    try:
        base_topic = Topic.objects.get(id=topic_id)
        sub_topics = SubTopic.objects.filter(topic_id=base_topic)
        if base_topic is not None:
            paginator = Paginator(sub_topics, 2)
            page = request.GET.get('page',1)
            sub_topics = paginator.page(page)
            return render(request, 'topic.html',{"base_topic":base_topic,'sub_topics':sub_topics})

    except Topic.DoesNotExist as e:
        return render(request, '404.html', status=404)

#create sub topic
def create_subtopic(request):
    if request.user.is_authenticated:
        text = request.POST.get('text','')
        base_topic = request.POST.get('sub_topic','')
        base_topic = Topic.objects.get(id=base_topic)
        try:

            topic = SubTopic()
            topic.author = request.user
            topic.pub_date = now()
            topic.text = text
            topic.topic = base_topic
            topic.save()
            messages.add_message(request, messages.SUCCESS, "Добавихте успешно отговор")
        except ValueError as e:
            messages.add_message(request, messages.WARNING, str(e[0]))

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


#GET profile page
@login_required
def profile_page(request):
    try:
        avatar = Avatar.objects.get(user_id=request.user.id)
    except Avatar.DoesNotExist as e:
        avatar = None
    return render(request, "profile.html",{"avatar":avatar})


#POST request
@login_required
def upload_profile_avatar(request):
    if request.method == "POST":

        profile = ProfileForm(request.POST,request.FILES)
        Avatar.objects.filter(user_id=request.user.id).delete()
        if profile.is_valid():

            avatar = Avatar()
            avatar.image = profile.cleaned_data["image"]
            avatar.user = request.user
            avatar.save()


    return redirect('/profile')
























