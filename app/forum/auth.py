# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth import authenticate, login,logout
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.core.exceptions import ValidationError



# login and register pages

#GET login
def auth_login_page(request):
    return render(request,"login.html")

#POST login
def auth_login(request):
    if  request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
        else:
            messages.add_message(request, messages.ERROR, 'Wrong username or password')
            return render(request, "login.html")

    return redirect("/")
#GET register
def auth_register_page(request):
    return render(request,"register.html")

#POST register
def auth_register(request):

    username = request.POST.get('username','')
    email = request.POST.get('email','')
    password = request.POST.get('password','')
    re_password = request.POST.get('re_password','')

    try:
        if User.objects.filter(username=username).count() > 0:
            raise ValueError("username is busy")

        if User.objects.filter(email=email).count() > 0:
            raise ValueError("email is busy")

        if len(username) < 3:
            raise ValueError("Username is too short")
        if len(password) < 6:
            raise ValueError("Password is too short")

        if password != re_password:
            raise ValueError("Password mismatch")


        if validate_email(email) is  None:
            #if validation is ok create new user
            user = User.objects.create_user(username=username,
                                            email=email,
                                            password=password)
        return render(request, "login.html")
    except ValueError as e:
        messages.add_message(request, messages.ERROR, str(e))
    except ValidationError as e:
        messages.add_message(request, messages.ERROR, str(e[0]))

    return render(request, "register.html")

def auth_logout(request):
    logout(request)
    return render(request, "login.html")