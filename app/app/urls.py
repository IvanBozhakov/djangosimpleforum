
from django.conf.urls import url, include
from django.contrib import admin
from forum import auth
from forum import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', auth.auth_login_page),
    url(r'^register/', auth.auth_register_page, name='register'),
    url(r'^auth/register/', auth.auth_register),
    url(r'^auth/login/', auth.auth_login),
    url(r'^auth/logout/', auth.auth_logout),

    url(r'^forum/',include('forum.urls')),

    url(r'^profile', views.profile_page),
    url(r'^upload/image',views.upload_profile_avatar),

    url(r'^$', views.index),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



